import openpyxl
import gensim
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import pymorphy2
import csv


stop_words = stopwords.words("russian")
stop_plus = ['.',',',';','?','!',':','из-за','вроде', 'затем', 'зато', 'итак', 'пока', 'поскольку', 'причем', 'также',
             'вне', 'изо', 'кроме', 'обо', 'перед', 'по', 'сквозь', 'оно', 'ваш', 'наш', 'свой', 'это', 'чей', 'б']
stop_words.extend(stop_plus)

# get data from excel
wb = openpyxl.load_workbook('test_bio_astro_film.xlsx')
sheet = wb['Лист1']
rows = sheet.max_row


bio_dataset=[]
bio_dataset_test = []
astro_dataset=[]
astro_dataset_test=[]
film_dataset=[]
film_dataset_test=[]

for i in range(1,rows+1):
    cell_text = sheet.cell(row=i, column=1).value
    if cell_text != None:
        bio_dataset.append(cell_text)
    cell_text = sheet.cell(row=i, column=2).value
    if cell_text != None:
        bio_dataset_test.append(cell_text)
    cell_text = sheet.cell(row=i, column=3).value
    if cell_text != None:
        astro_dataset.append(cell_text)
    cell_text = sheet.cell(row=i, column=4).value
    if cell_text != None:
        astro_dataset_test.append(cell_text)
    cell_text = sheet.cell(row=i, column=5).value
    if cell_text != None:
        # print("ok")
        film_dataset.append(cell_text)
    cell_text = sheet.cell(row=i, column=6).value
    if cell_text != None:
        film_dataset_test.append(cell_text)

# make dataset

categories = ["biology", "astronomy", "films"]
cat_dict = {} # Contains raw training data organized by category

cat_dict_test = {} # test data organized by category

cat_dict[categories[0]] = bio_dataset
cat_dict[categories[1]] =astro_dataset
cat_dict[categories[2]] =film_dataset
cat_dict_test[categories[0]] = bio_dataset_test
cat_dict_test[categories[1]] =astro_dataset_test
cat_dict_test[categories[2]] =film_dataset_test


# prepare dataset

morph = pymorphy2.MorphAnalyzer()
#snowball = SnowballStemmer(language="russian")
def my_tokenize(text):
    a = [morph.parse(token)[0].normal_form for token in word_tokenize(text, language="russian") if token.lower() not in stop_words]
    return a


cat_dict_tagged_train = {}  # clean training data organized by category.Used for the training corpus
cat_dict_test_clean = {}  # clean un-tagged training data organized by category


offset = 0 # Used for managing IDs of tagged documents
for k, v in cat_dict.items():
    cat_dict_tagged_train[k] = [gensim.models.doc2vec.TaggedDocument(my_tokenize(text), [i+offset]) for i, text in enumerate(v)]
    offset += len(v)


offset = 0
for k, v in cat_dict_test.items():
    cat_dict_test_clean[k] = [my_tokenize(text) for i, text in enumerate(v)]
    offset += len(v)


# Eventually contains final versions of the training data to actually train the model
train_corpus = [taggeddoc for taggeddoc_list in list(cat_dict_tagged_train.values()) for taggeddoc in taggeddoc_list]

# print(train_corpus)

# learning
model_dbow = gensim.models.doc2vec.Doc2Vec(dm =0, vector_size=60, min_count=3, epochs=60, window=2)

model_dm = gensim.models.doc2vec.Doc2Vec(dm =1, vector_size=60, min_count=3, epochs=60, window=2)

model_dbow.build_vocab(train_corpus)
model_dm.build_vocab(train_corpus)

model_dbow.train(train_corpus, total_examples=model_dbow.corpus_count, epochs=model_dbow.epochs)
model_dm.train(train_corpus, total_examples=model_dm.corpus_count, epochs=model_dm.epochs)
# print(len(model.wv))

# tests
metadata_dbow = {}
inferred_vectors_test_dbow = {} # Contains, category-wise, inferred doc vecs for each document in the test set
for cat, docs in cat_dict_test_clean.items():
    inferred_vectors_test_dbow[cat] = [model_dbow.infer_vector(doc) for doc in list(docs)]
    metadata_dbow[cat] = len(inferred_vectors_test_dbow[cat])

metadata_dm = {}
inferred_vectors_test_dm = {} # Contains, category-wise, inferred doc vecs for each document in the test set
for cat, docs in cat_dict_test_clean.items():
    inferred_vectors_test_dm[cat] = [model_dm.infer_vector(doc) for doc in list(docs)]
    metadata_dm[cat] = len(inferred_vectors_test_dm[cat])


def write_to_csv(input, output_file, delimiter='\t'):
    with open(output_file, "w") as f:
        writer = csv.writer(f, delimiter=delimiter)
        writer.writerows(input)


veclist_metadata_dbow = []
veclist_metadata_dm = []
veclist_dbow = []
veclist_dm = []

for cat in cat_dict.keys():
    for tag in [cat] * metadata_dbow[cat]:
        veclist_metadata_dbow.append([tag])
    for vec in inferred_vectors_test_dbow[cat]:
        veclist_dbow.append(list(vec))

write_to_csv(veclist_dbow, "vect_dbow.csv")
write_to_csv(veclist_metadata_dbow, "meta_dbow.csv")

for cat in cat_dict.keys():
    for tag in [cat] * metadata_dm[cat]:
        veclist_metadata_dm.append([tag])
    for vec in inferred_vectors_test_dm[cat]:
        veclist_dm.append(list(vec))

write_to_csv(veclist_dm, "vect_dm.csv")
write_to_csv(veclist_metadata_dm, "meta_dm.csv")

